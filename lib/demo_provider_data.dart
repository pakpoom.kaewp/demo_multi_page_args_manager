import 'package:flutter/material.dart';

class DemoProviderData extends ChangeNotifier {
  String myData;

  void setMyData(String userProvidedMyData) {
    print('Set my data called');
    myData = userProvidedMyData;
    print('- My data had set');
    notifyListeners();
  }

  String getMyData() {
    print('Get my data called');
    return myData;
  }
}
