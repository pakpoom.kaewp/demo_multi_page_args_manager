import 'package:demo_multi_page_args_manager/demo_provider_data.dart';
import 'package:demo_multi_page_args_manager/detail_screen.dart';
import 'package:demo_multi_page_args_manager/detail_with_provider_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:js' as js;
import 'dart:html' as html;

import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = '/home';
  @override
  Widget build(BuildContext context) {
    const String url = 'http://localhost/#/detail';
    const String urlDetailWithProvider =
        'http://localhost/#/detailWithProvider';
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  Provider.of<DemoProviderData>(context, listen: false)
                      .setMyData('Data 1');
                },
                child: Text('*** Set my data'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, DetailScreen.routeName);
                },
                child: Text('Go to detail page'),
              ),
              ElevatedButton(
                onPressed: () {
                  js.context.callMethod('open', [url]);
                },
                child: Text('Go to detail page (js)'),
              ),
              ElevatedButton(
                onPressed: () {
                  html.window.open(url, 'new tab');
                },
                child: Text('Go to detail page (html)'),
              ),
              ElevatedButton(
                onPressed: () async {
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                child: Text('Go to detail page (url launcher)'),
              ),
              ElevatedButton(
                onPressed: () async {
                  if (await canLaunch(url)) {
                    await launch(
                      url,
                      forceSafariVC: true,
                      forceWebView: true,
                      webOnlyWindowName: '_self',
                    );
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                child: Text('Go to detail page (url launcher [_self])'),
              ),
              ElevatedButton(
                onPressed: () async {
                  if (await canLaunch(url)) {
                    await launch(
                      url,
                      forceSafariVC: true,
                      forceWebView: true,
                      webOnlyWindowName: '_blank',
                    );
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                child: Text('Go to detail page (url launcher [_blank])'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(
                      context, DetailWithProviderScreen.routeName);
                },
                child: Text('Go to detail with provider page'),
              ),
              ElevatedButton(
                onPressed: () {
                  js.context.callMethod('open', [urlDetailWithProvider]);
                },
                child: Text('Go to detail with provider page (js)'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
