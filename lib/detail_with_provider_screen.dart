import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'demo_provider_data.dart';

class DetailWithProviderScreen extends StatelessWidget {
  static const routeName = '/detailWithProvider';
  @override
  Widget build(BuildContext context) {
    String myData =
        Provider.of<DemoProviderData>(context, listen: false).getMyData();
    return ChangeNotifierProvider(
      create: (context) => DemoProviderData(),
      lazy: false,
      child: Scaffold(
        body: Container(
          child: Center(
            child: Text(myData),
          ),
        ),
      ),
    );
  }
}
