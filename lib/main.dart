import 'package:demo_multi_page_args_manager/demo_provider_data.dart';
import 'package:demo_multi_page_args_manager/detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'detail_with_provider_screen.dart';
import 'home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => DemoProviderData(),
      lazy: false,
      child: MaterialApp(
        theme: ThemeData.dark().copyWith(scaffoldBackgroundColor: Colors.green),
        routes: {
          HomeScreen.routeName: (context) => HomeScreen(),
          DetailScreen.routeName: (context) => DetailScreen(),
          DetailWithProviderScreen.routeName: (context) =>
              DetailWithProviderScreen(),
        },
        home: HomeScreen(),
      ),
    );
  }
}
