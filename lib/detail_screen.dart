import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:demo_multi_page_args_manager/demo_provider_data.dart';

class DetailScreen extends StatelessWidget {
  static const routeName = '/detail';
  @override
  Widget build(BuildContext context) {
    String myData =
        Provider.of<DemoProviderData>(context, listen: false).getMyData();
    return Scaffold(
      body: Container(
        child: Center(
          child: Text(myData),
        ),
      ),
    );
  }
}
